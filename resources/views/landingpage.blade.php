<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	@include('partials.styles')
	<title>Document</title>
</head>

<body>
	<div class="container w-75">
		<nav class="navbar fixed-top navbar-expand-lg bg-transparant px-5">
			<div class="container w-75">
				<a class="navbar-brand" href="#">
					<img src="{{ asset('img/white-logo.png') }}" alt="Jobseeker logo">
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
					aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link btn btn-primary btn-block btn-sm" aria-current="page" href="#">Contact
								Us</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#"><span class="fw-bold">English</span>/Indonesia</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		{{-- hero --}}
		<section class="hero d-flex flex-column justify-content-center align-items-center mb-5">
			<div class="row w-50">
				<div class="col-sm-12 text-center text-white">
					<h2 class="mb-4 fw-bold">Connecting people with opportunities</h2>
					<p class="mb-4">Kami percaya setiap orang berhak memiliki kesempatan untuk berkarya.
						Jobseeker Company hadir,
						untuk membantu menghubungkan mereka dengan kesempatan berkembang demi kehidupan yang lebih baik.
					</p>
					<button class="btn btn-primary py-3">See Our Products</button>
				</div>
			</div>
		</section>

		{{-- products --}}
		<section class="mb-5">
			<h1 class="text-center fw-bold mb-3">Our Products</h1>
			<div class="card shadow text-black mb-3" style="background-color: #F0EEFE">
				<div class="card-body text-center p-5">
					<div class="container w-50">
						<h3 class="fw-bold text-gradient">For Candidate</h3>
						<h3 class="fw-bold text-black my-3">
							<span>
								<img src="{{ asset('img/colored-logo.png') }}" alt="logo">
							</span>
							jobseeker.app
						</h3>
						<p class="mb-3">
							Social recruitment platform untuk para pencari kerja, terutama untuk non white-collar worker
							yang
							jumlahnya 4-5x lebih banyak daripada angkatan kerja white collar. Jobseker.App memungkinkan
							pencari kerja untuk mencari kerja secara lebih mudah, cepat, dan sesuai dengan lokasi
							mereka.
						</p>
						<a href="#" class="fw-semibold text-decoration-none">Learn More -></a>
					</div>
				</div>
			</div>

			<div class="card shadow text-black mb-3" style="background-color: #ECF5FF">
				<div class="card-body text-center p-5">
					<div class="container w-50">
						<h3 class="fw-bold text-gradient">For Employer</h3>
						<h3 class="fw-bold text-black my-3">
							<span>
								<img src="{{ asset('img/colored-logo.png') }}" alt="logo">
							</span>
							jobseeker.partner
						</h3>
						<p class="mb-3">
							Indonesia memasuki era keemasan bonus demografi yang diprediksi mencapai puncaknya pada
							2030. Manfaatkan peluang ini untuk mendapatkan tenaga kerja terbaik dengan mudah, cepat,
							terdekat dari lokasi usaha.
						</p>
						<a href="#" class="fw-semibold text-decoration-none">Learn More -></a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="card shadow text-black mb-3" style="background-color: #ECF5FF; min-height: 350px">
						<div class="card-body text-center p-5">
							<div class="container">
								<h3 class="fw-bold text-black my-3">
									<span>
										<img src="{{ asset('img/colored-logo.png') }}" alt="logo">
									</span>
									jobseeker.software
								</h3>
								<p class="mb-3">
									99% perusahaan Fortune 500 menggunakan
									Applicant Tracking System (ATS) dalam strategi rekrutmen mereka. Software ATS
									membantu anda melakukan proses rekrutmen dari awal hingga akhir secara efektif dan
									efisien.
								</p>
								<a href="#" class="fw-semibold text-decoration-none">Learn More -></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card shadow text-black mb-3" style="background-color: #ECF5FF; min-height: 350px">
						<div class="card-body text-center p-5">
							<div class="container">
								<h3 class="fw-bold text-black my-3">
									<span>
										<img src="{{ asset('img/colored-logo.png') }}" alt="logo">
									</span>
									jobseeker.services
								</h3>
								<p class="mb-3">
									jobseekers.services memiliki akses ke jutaan kandidat dan talent pool untuk proses
									rekruitmen karyawan yang lebih cepat dengan harga yang lebih terjangkau.
								</p>
								<a href="#" class="fw-semibold text-decoration-none">Learn More -></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		{{-- features --}}
		<section class="features mb-5">
			<div class="container" style="max-width: 852px; padding: 75px 0;">
				<div class="yt-video mb-5">
					<iframe width="852" height="494" src="https://www.youtube.com/embed/DgqDDI2R9m4" title="YouTube video player"
						frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen>
					</iframe>
				</div>
				<div class="row text-white">
					<div class="col-sm-4">
						<h3 class="fw-bold">About Us</h3>
					</div>
					<div class="col-sm-8">
						<p class="text-start">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros
							elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut
							commodo
							diam libero vitae erat.
						</p>
					</div>
				</div>
			</div>
		</section>

		{{-- cultures --}}
		<section class="cultures mb-5">
			<div class="container" style="max-width: 85%">
				<div class="row d-flex flex-row align-items-center">
					<div class="col-sm-5">
						<figure>
							<img src="{{ asset('img/culture-img.png') }}" alt="culture image" class="img-fluid">
						</figure>
					</div>
					<div class="col-sm-7">
						<h2 class="text-black fw-bold">Our Culture</h2>
						<p class="text-muted text-start">
							Though we move at a fast pace, and it might seem that we don’t have time to sit and explain
							things, every employee at Jobseeker Company loves sharing, helping and lifting others. We
							have unique set of values on clear goals that define success.
						</p>
						<button class="btn btn-secondary py-2 px-4">Join Us</button>
					</div>
				</div>
			</div>
		</section>

		{{-- vacants --}}
		<section class="vacant mb-5">
			<div class="container" style="max-width: 85%">
				<h1 class="text-black fw-semibold mb-3">Join Our Team</h1>
				<p class="text-muted">
					We're building the first social recruitment platform in Indonesia. Jobseeker Company is perfect for
					those who:
				</p>
				<ul class="text-muted mb-5">
					<li>Want to work in a fast-paced startup</li>
					<li>Love having a sense of ownership to your projects</li>
					<li>Want to take part in building the next big startup in Indonesia</li>
				</ul>

				<h5 class="text-black fw-semibold mb-4">CURRENT POSITIONS</h5>

				<div class="row">
					<div class="col-sm-3">
						<dl>
							<dt class="text-decoration-underline">All Locations</dt>
							<dt class="text-muted">Jakarta</dt>
							<dt class="text-muted">Malang</dt>
						</dl>
					</div>
					<div class="col-sm-9">
						<div class="card shadow mb-4">
							<div class="card-body p-5">
								<div class="d-flex flex-row align-items-center justify-content-between mb-4">
									<h2 class="text-black fw-bold mr-4">Marketing Manager</h2>
									<img src="{{ asset('img/location.png') }}" alt="location icon" class="mr-4">
									<h6 class="text-primary">Jakarta</h6>
									<button class="btn btn-secondary">Apply</button>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Requirements</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> Bachelor Degree </dt>
											<dt> Min. 5 years of experience </dt>
										</dl>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Salary</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> IDR 7 - 10 Millions </dt>
										</dl>
									</div>
								</div>
								<p class="text-muted">24 mins ago</p>
							</div>
						</div>
						<div class="card shadow mb-4">
							<div class="card-body p-5">
								<div class="d-flex flex-row align-items-center justify-content-between mb-4">
									<h2 class="text-black fw-bold mr-4">Frontend Developer</h2>
									<img src="{{ asset('img/location.png') }}" alt="location icon" class="mr-4">
									<h6 class="text-primary">Malang</h6>
									<button class="btn btn-secondary">Apply</button>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Requirements</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> Bachelor Degree </dt>
											<dt> Min. 2 years of experience </dt>
										</dl>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Salary</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> IDR 7 - 10 Millions </dt>
										</dl>
									</div>
								</div>
								<p class="text-muted">24 mins ago</p>
							</div>
						</div>
						<div class="card shadow mb-4">
							<div class="card-body p-5">
								<div class="d-flex flex-row align-items-center justify-content-between mb-4">
									<h2 class="text-black fw-bold mr-4">Backend Developer</h2>
									<img src="{{ asset('img/location.png') }}" alt="location icon" class="mr-4">
									<h6 class="text-primary">Jakarta</h6>
									<button class="btn btn-secondary">Apply</button>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Requirements</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> Bachelor Degree </dt>
											<dt> Min. 5 years of experience </dt>
										</dl>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<p class="text-muted">Salary</p>
									</div>
									<div class="col-sm-9">
										<dl class="text-muted">
											<dt> IDR 7 - 10 Millions </dt>
										</dl>
									</div>
								</div>
								<p class="text-muted">24 mins ago</p>
							</div>
						</div>
						<a href="#" class="text-decoration-none float-end">See all vacancies -></a>
					</div>
				</div>
			</div>
		</section>

		{{-- board directors --}}
		<section class="boards mb-5">
			<div class="container" class="d-flex" style="max-width: 75%">
				<h1 class="text-black text-center fw-bold mb-3">Board of Directors</h1>
				<div class="row justify-content-evenly">
					<div class="col-sm-4">
						<img src="{{ asset('img/ceo.png') }}" alt="" class="img-fluid mb-3" style="border-radius: 50%">
						<h4 class="text-black fw-bold text-center">Chandra Ming</h4>
						<h5 class="text-muted text-center">Founder & CEO</h5>
					</div>
					<div class="col-sm-4">
						<img src="{{ asset('img/coo.png') }}" alt="" class="img-fluid mb-3" style="border-radius: 50%">
						<h4 class="text-black fw-bold text-center">Juliana Siburian</h4>
						<h5 class="text-muted text-center">Co-Founder & COO</h5>
					</div>
					<div class="col-sm-4">
						<img src="{{ asset('img/cfo.png') }}" alt="" class="img-fluid mb-3" style="border-radius: 50%">
						<h4 class="text-black fw-bold text-center">Alankar Joshi</h4>
						<h5 class="text-muted text-center">Co-Founder & CFO</h5>
					</div>
				</div>
			</div>
		</section>

		{{-- footer --}}
		<footer>
			<div class="container px-5 py-4 pb-5">
				<div class="row text-white">
					<div class="col-sm-6">
						<img src="{{ asset('img/white-logo.png') }}" alt="jobseeker logo" class="img-fluid mb-4">
						<p class="fw-bold mb-0">Singapore</p>
						<p>10 Anson Road #22-02, International Plaza, Singapore, 079903</p>
						<p class="fw-bold mb-0">Indonesia</p>
						<p>
							AD Premier Office Park, 9th Floor
							Jl. TB Simatupang No.5, Ragunan, Pasar Minggu
							South Jakarta City, Jakarta 12550
						</p>
						<span>
							<img src="{{ asset('img/mail.png') }}" alt="jobseeker email" class="me-4">
							<a href="mailto:info@jobseeker.company" class="text-decoration-none text-white">
								info@jobseeker.company</a>
						</span>
						<br class="mb-2">
						<span>
							<img src="{{ asset('img/whatsapp.png') }}" alt="jobseeker whatsapp" class="me-4">
							<a href="http://wa.me/6281318817887" class="text-decoration-none text-white" target="_blank">
								+62813 1881 7887</a>
						</span>
						<br class="mb-4">
						<span>
							<img src="{{ asset('img/facebook.png') }}" alt="jobseeker facebook" class="me-2">
							<img src="{{ asset('img/instagram.png') }}" alt="jobseeker instagram" class="me-2">
							<img src="{{ asset('img/linkedin.png') }}" alt="jobseeker linkedin" class="me-2">
							<img src="{{ asset('img/tiktok.png') }}" alt="jobseeker tiktok" class="me-2">
							<img src="{{ asset('img/youtube.png') }}" alt="jobseeker youtube" class="me-2">
							<img src="{{ asset('img/telegram.png') }}" alt="jobseeker telegram" class="me-2">
						</span>
					</div>
					<div class="col-sm-6 pt-5">
						<div class="row text-white">
							<div class="col-sm-4">
								jobseeker.company
							</div>
							<div class="col-sm-4">
								<p class="fw-bold">For Employer</p>
								<div class="mb-3">
									<p class="mb-0" style="color: white; opacity: .4;">jobseeker.partners</p>
									<small style="text-primary">(Coming Soon)</small>
								</div>
								<p class="">jobseeker.services</p>
								<p class="text-decoration-underline">jobseeker.software</p>
							</div>
							<div class="col-sm-4">
								<p class="fw-bold">For Candidate</p>
								<div class="mb-3">
									<p class="mb-0" style="color: white; opacity: .4;">jobseeker.app</p>
									<small style="text-primary">(Coming Soon)</small>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="float-start">
					<p class="text-white">Copyright © 2022 JobSeekerApps Pte. Ltd</p>
				</div>
				<div class="float-end">
					<a href="#" class="text-white">Privacy Policy</a>
					<a href="#" class="text-white">Terms of Service</a>
				</div>

			</div>
		</footer>
	</div>

	@include('partials.scripts')
</body>

</html>
