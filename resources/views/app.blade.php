<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	@include('partials.styles')
	<title>Document</title>
</head>

<body>
	<div class="container">
		@include('partials.header')
		<div class="content">
			@yield('content')
		</div>
	</div>

	@include('partials.scripts')
</body>

</html>
