@extends('app')

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<button type="button" class="btn btn-primary float-end mb-5" id="createInventory" data-bs-toggle="modal"
				data-bs-target="#inventory" onclick="createInventory()">
				Insert New Item
			</button>

			<table class="table" id="inventoryTable">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Goods</th>
						<th scope="col">Category</th>
						<th scope="col">Price</th>
						<th scope="col">action</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>

	{{-- modal --}}
	<!-- Modal -->
	<div class="modal fade" id="inventory" tabindex="-1" aria-labelledby="inventoryLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="inventoryLabel">Insert Inventory</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<form action="{{ route('inventory.store') }}" method="post" id="inventoryForm">
						@csrf
						<div class="mb-3">
							<label for="good" class="form-label">Good</label>
							<input type="text" name="good" class="form-control" id="good" autofocus required>
						</div>
						<div class="mb-3">
							<label for="price" class="form-label">Price</label>
							<input type="text" name="price" class="form-control" id="price" required>
						</div>
						<div class="mb-3">
							<label for="category_code" class="form-label">Category</label>
							<select name="category_code" id="category_code" class="form-control" required>
								@isset($categories)
									@foreach ($categories as $category)
										<option value="{{ $category->category_code }}">{{ $category->category }}</option>
									@endforeach
								@endisset
							</select>
						</div>
						<div class="mb-3">
							<label for="stock" class="form-label">Stock</label>
							<input type="text" name="stock" class="form-control" id="stock" required>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('page_scripts')
	<script>
	 $(document).ready(function() {
	  var columns = [{
	    data: 'DT_RowIndex',
	    orderable: false,
	    searchable: false,
	   },
	   {
	    data: 'good',
	    name: 'good'
	   }, {
	    data: 'category',
	    name: 'category'
	   }, {
	    data: 'price',
	    name: 'price'
	   }, {
	    data: 'action',
	    name: 'action',
	    orderable: false,
	    searchable: false,
	   },
	  ]
	  var table = initiateDatatable('inventoryTable', "{{ route('inventory.index') }}", columns);

	  $('#inventoryForm').submit(function(e) {
	   e.preventDefault();
	   var form = $(this);
	   var url = form.attr('action');
	   var method = form.attr('method');
	   var data = form.serialize();
	   $.ajax({
	    url: url,
	    method: method,
	    data: data,
	    success: function(data) {
	     $('#inventory').modal('hide');
	     //  table.ajax.reload();
	     table.draw();
	    }
	   });
	  })
	 })
	</script>
@endsection
