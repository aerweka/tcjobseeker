<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\InventoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingpage');
});

Route::group([
    'prefix' => 'category',
    'as' => 'category.',
], function () {
    Route::get('/', [CategoryController::class, 'index'])->name('index');
    Route::get('/create', [CategoryController::class, 'create'])->name('create');
    Route::post('/store', [CategoryController::class, 'store'])->name('store');
    Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('edit');
    Route::post('/{id}/update', [CategoryController::class, 'update'])->name('update');
    Route::get('/{id}/delete', [CategoryController::class, 'delete'])->name('delete');
});

Route::group([
    'prefix' => 'inventory',
    'as' => 'inventory.',
], function () {
    Route::get('/', [InventoryController::class, 'index'])->name('index');
    Route::get('/create', [InventoryController::class, 'create'])->name('create');
    Route::post('/store', [InventoryController::class, 'store'])->name('store');
    Route::get('/{id}/edit', [InventoryController::class, 'edit'])->name('edit');
    Route::post('/{id}/update', [InventoryController::class, 'update'])->name('update');
    Route::get('/{id}/delete', [InventoryController::class, 'delete'])->name('delete');
});

Route::get('/latest-ending-stock', [InventoryController::class, 'getLatestInventoryStock']);
