let table = null;
function initiateDatatable(tableId, url, columns) {
    // datatable options
    var dtOptions = {
        bLengthChange: false, // this gives option for changing the number of records shown in the UI table
        lengthMenu: [10], // 4 records will be shown in the table
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: url,
        columns: columns,
    };

    table = $("#" + tableId).DataTable(dtOptions);
    return table;
}

function deleteConfirmation() {
    var msg = confirm("Data akan terhapus permanen. Apakah akan dilanjutkan?");

    if (!msg) {
        return false;
    }
}

function editInventory(id) {
    $("#editInventory").on("click", function () {
        console.log("clicked");
        $("#inventoryLabel").html("Edit Inventory");

        // var id = $(this).data("id");

        $.get(`/inventory/${id}/edit`, function (data) {
            var { inventory } = data;
            $("#good").val(inventory.good);
            $("#price").val(inventory.price);
            $("#category_code").val(inventory.category_code);
            $("#stock").val(inventory.stock);
            $("#inventoryForm").attr(
                "action",
                `/inventory/${inventory.inventory_code}/update`
            );
        });
    });
}

function createInventory() {
    $("#inventoryForm").attr("action", "{{ route('inventory.store') }}");
    $("#inventoryLabel").html("Insert Inventory");
    $("#good").val(inventory.good);
    $("#price").val(inventory.price);
    $("#category_code").val(inventory.category_code);
    $("#stock").val(inventory.stock);
}

function deleteInventory(id) {
    console.log("clicked");
    if (deleteConfirmation() == false) return false;
    $.get(`/inventory/${id}/delete`, function (data) {
        table.draw();
    });
}

module.export = {
    initiateDatatable,
    deleteConfirmation,
    editInventory,
    createInventory,
    deleteInventory,
};
