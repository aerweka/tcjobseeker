<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InventoriyRequest;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\StockHistory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class InventoryController extends Controller
{
    private $minimumPriceToBeDiscounted = 40000;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Inventory::join('categories', 'categories.category_code', 'inventories.category_code')->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="text-black" id="editInventory" data-bs-toggle="modal" data-bs-target="#inventory" data-id="' . $row->inventory_code . '" onclick="editInventory(' . $row->inventory_code . ')">Edit</a> | <a href="javascript:void(0)" data-id="' . $row->inventory_code . '" class="text-black" id="deleteInventory" onclick="deleteInventory(' . $row->inventory_code . ')">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        $categories = Category::all();

        return view('inventory.index', compact('latestInventoryStock', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InventoriyRequest $request)
    {
        $validatedData = $request->validated();

        if ($validatedData['price'] > $this->minimumPriceToBeDiscounted) ($validatedData['price'] * 10) / 100;
        if (Inventory::where('good', $validatedData['good'])->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Inventory already exists.'
            ]);
        }

        Inventory::create($validatedData);

        return response()->json([
            'success' => true,
            'message' => 'Inventory stored successfully',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventory = Inventory::findOrFail($id);

        return response()->json([
            'success' => true,
            'inventory' => $inventory,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InventoriyRequest $request, $id)
    {
        $validatedData = $request->validated();

        $inventory = Inventory::findOrFail($id);

        $inventory->update($validatedData);

        return response()->json([
            'success' => true,
            'message' => 'Inventory updated successfully',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $inventory = Inventory::findOrFail($id);

        $inventory->delete();

        return response()->json([
            'success' => true,
            'message' => 'Inventory deleted successfully',
        ]);
    }

    /**
     * Retrieve latest stock history for each inventory item for a day.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLatestInventoryStock()
    {
        $latestInventory = StockHistory::select('inventory_code', DB::raw('SUM(final_stock) as final_stock'))->where('date', Carbon::now())->groupBy('inventory_code')->get();

        // $latestInventory = DB::raw('select inventory_code, final_stock, max(date) as max_date from stock_histories group by inventory_code) as latest_stock where latest_stock.inventory_code = stock_histories.inventory_code and latest_stock.max_date = stock_histories.date');

        return response()->json($latestInventory);
    }
}
