<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    use HasFactory;

    protected $fillable = ['stock_code', 'date', 'inventory_code', 'final_stock'];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_code');
    }
}
