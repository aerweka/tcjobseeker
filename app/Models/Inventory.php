<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $primaryKey = 'inventory_code';

    protected $fillable = ['good', 'price', 'category_code', 'stock'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_code');
    }

    public function stock_histories()
    {
        return $this->hasMany(StockHistory::class);
    }
}
